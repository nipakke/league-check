var socket = io();
socket.on('connect', function(data) {

});

var app = angular.module('leagueCheckApp', []);
app.controller('leagueCheckCtrl', function($scope, $http) {
  $scope.summonerName = '';
  $scope.caption = "League Checker";
  $scope.server = 'na1';
  $scope.hideTable = true;
  $scope.hideLoading = true;
  $scope.hideMessage = true;
  $scope.hideInput = false;
  $scope.draft = true;
  $scope.cv = 0;
  $scope.match = "";
  $scope.rangok = "";

  socket.emit('checkApiEmit');
  socket.on('checkApiSocket', function (){
    $scope.$apply(function() { $scope.caption = "BAD API KEY"; });
    $scope.$apply(function() { $scope.hideInput = true; });
  });


  socket.on('noSummoner', function (data) {
    $scope.$apply(function() { $scope.message = "Summoner not found!"; });
    $scope.$apply(function() { $scope.hideLoading = true; });
    $scope.$apply(function() { $scope.hideMessage = false; });
    $scope.$apply(function() { $scope.hideTable = true; });
  });

  socket.on('noMatch', function (data) {
    $scope.$apply(function() { $scope.message = "Summoner is not in game!"; });
    $scope.$apply(function() { $scope.hideLoading = true; });
    $scope.$apply(function() { $scope.hideMessage = false; });
    $scope.$apply(function() { $scope.hideTable = true; });
  });

  socket.on('match', function (data) {
    $scope.$apply(function() { $scope.hideTable = false; });
    $scope.$apply(function() { $scope.hideLoading = true; });
    $scope.rangok[$scope.cv] = data.kocsogok;
    $scope.match = data.match;
    $scope.cv += 1;
    if($scope.cv == 10){ $scope.cv = 0; }
    $scope.$apply(function() { $scope.rangok = $scope.rangok; });
    $scope.$apply(function() { $scope.cv = $scope.cv; });
    if ($scope.match.bannedChampions.length == 0){
      $scope.$apply(function() { $scope.draft = false; });
    }

  });

  $scope.searchFunc = function(){
    $scope.hideTable = true;
    $scope.hideMessage = true;
    $scope.hideLoading = false;
    socket.emit('search',{
      'summonerName': $scope.summonerName,
      'server': $scope.server
    });

  };
});


//End of ngjs

$(document).ready(function() {
    $('select').material_select();
  });


$('.sel').each(function() {
  $(this).children('select').css('display', 'none');

  var $current = $(this);

  $(this).find('option').each(function(i) {
    if (i == 0) {
      $current.prepend($('<div>', {
        class: $current.attr('class').replace(/sel/g, 'sel__box')
      }));

      var placeholder = $(this).text();
      $current.prepend($('<span>', {
        class: $current.attr('class').replace(/sel/g, 'sel__placeholder'),
        text: placeholder,
        'data-placeholder': placeholder
      }));

      return;
    }

    $current.children('div').append($('<span>', {
      class: $current.attr('class').replace(/sel/g, 'sel__box__options'),
      text: $(this).text()
    }));
  });
});

$('.sel').click(function() {
  $(this).toggleClass('active');
});

$('.sel__box__options').click(function() {
  var txt = $(this).text();
  var index = $(this).index();

  $(this).siblings('.sel__box__options').removeClass('selected');
  $(this).addClass('selected');

  var $currentSel = $(this).closest('.sel');
  $currentSel.children('.sel__placeholder').text(txt);
  $currentSel.children('select').prop('selectedIndex', index + 1);
});
