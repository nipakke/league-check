var express = require('express');
var app = require('express')();
var server = require('http').Server(app);
var io = require('socket.io')(server);
var config = require('./config.js');
var apiKey = config.apiKey;
var request = require('request');
var sync = require('sync');

app.set('view engine', 'ejs');
app.use(express.static('views/public'));
app.get('/', function(req, res) {
    res.render('pages/index');
});

app.get('/about', function(req, res) {
    res.render('pages/about');
});

app.use(function(req, res) {
      res.status(400);
     res.render('pages/404', {title: '404: File Not Found'});
  });

  // Handle 500
  app.use(function(error, req, res, next) {
      res.status(500);
     res.render('pages/500', {title:'500: Internal Server Error', error: error});
  });

//CHECKAPIKEY FUNCTION KEZDETE
function checkApiFunc(callback){
  var options = {
    url: 'https://na1.api.riotgames.com/lol/platform/v3/third-party-code/by-summoner/checkApi' ,
    headers: {
      "Origin": "https://developer.riotgames.com",
      "Accept-Charset": "application/x-www-form-urlencoded; charset=UTF-8",
      "X-Riot-Token": apiKey,
      "Accept-Language": "hu-HU,hu;q=0.9,en-US;q=0.8,en;q=0.7,sk;q=0.6,es;q=0.5,und;q=0.4,ru;q=0.3",
      "User-Agent": "Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/64.0.3282.140 Safari/537.36"
    }
  };

  request(options, function (error, response, body) {
      process.nextTick(function(){
          callback(null, JSON.parse(body));
  	  })
  });
}
//CHECKAPIKEY FUNCTION VÉGE
//GETSUMMONERID FUNCTION KEZDETE
function getSummoner(summonerName, server, callback){
  var options = {
    url: 'https://' + server + '.api.riotgames.com/lol/summoner/v3/summoners/by-name/' + summonerName,
    headers: {
      "Origin": "https://developer.riotgames.com",
      "Accept-Charset": "application/x-www-form-urlencoded; charset=UTF-8",
      "X-Riot-Token": apiKey,
      "Accept-Language": "hu-HU,hu;q=0.9,en-US;q=0.8,en;q=0.7,sk;q=0.6,es;q=0.5,und;q=0.4,ru;q=0.3",
      "User-Agent": "Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/64.0.3282.140 Safari/537.36"
    }
  };

  request(options, function (error, response, body) {
      process.nextTick(function(){
        if(response.statusCode == 200){
          callback(null, JSON.parse(body).id);
        }else{
          callback(null, 0);
        }
  	  })
  });
}
//GETSUMMONERID FUNCTION VÉGE
//GETMATCH FUNCTION KEZDETE
function getMatch(summonerId, server, callback){
  var options = {
    url: 'https://' + server + '.api.riotgames.com/lol/spectator/v3/active-games/by-summoner/' + summonerId,
    headers: {
      "Origin": "https://developer.riotgames.com",
      "Accept-Charset": "application/x-www-form-urlencoded; charset=UTF-8",
      "X-Riot-Token": apiKey,
      "Accept-Language": "hu-HU,hu;q=0.9,en-US;q=0.8,en;q=0.7,sk;q=0.6,es;q=0.5,und;q=0.4,ru;q=0.3",
      "User-Agent": "Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/64.0.3282.140 Safari/537.36"
    }
  };
  request(options, function (error, response, body) {
      process.nextTick(function(){

        if(response.statusCode == 200){
          callback(null, JSON.parse(body));
        }else{
          callback(null, 0);
        }

  	  })
  });
}
//GETMATCH FUNCTION VÉGE
//GETLEAGUE FUNCTION KEZDETE
function getLeague(summonerId, server, callback){
  var options = {
    url: 'https://' + server + '.api.riotgames.com/lol/league/v3/positions/by-summoner/' + summonerId,
    headers: {
      "Origin": "https://developer.riotgames.com",
      "Accept-Charset": "application/x-www-form-urlencoded; charset=UTF-8",
      "X-Riot-Token": apiKey,
      "Accept-Language": "hu-HU,hu;q=0.9,en-US;q=0.8,en;q=0.7,sk;q=0.6,es;q=0.5,und;q=0.4,ru;q=0.3",
      "User-Agent": "Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/64.0.3282.140 Safari/537.36"
    }
  };
  request(options, function (error, response, body) {
      process.nextTick(function(){

        var info = "";
        for (var i = 0; i < JSON.parse(body).length; i++) {


          if (typeof JSON.parse(body)[i] !== 'undefined' && JSON.parse(body)[i] !== null)
            if(JSON.parse(body)[i].queueType == "RANKED_SOLO_5x5"){
              info = JSON.parse(body)[i];
            }
          }
         if (typeof info !== 'undefined' && info !== null)
           {
             switch(info.rank){
               case "I":
                 info.rank = "1";
                 break;
               case "II":
                 info.rank = "2";
                 break;
               case "III":
                 info.rank = "3";
                 break;
               case "IV":
                 info.rank = "4";
                 break;
               case "V":
                 info.rank = "5";
                 break;
               default:
                info.rank = "Unranked rank";
             }


             switch(info.tier){
               case "BRONZE":
                 info.tier = "Bronze";
                 break;
               case "SILVER":
                 info.tier = "Silver";
                 break;
               case "GOLD":
                 info.tier = "Gold";
                 break;
               case "PLATINUM":
                 info.tier = "Platinum";
                 break;
               case "DIAMOND":
                 info.tier = "Diamond";
                 break;
               case "MASTER":
                 info.tier = "Master";
                 break;
               case "CHALLENGER":
                 info.tier = "Challenger";
                 break;
               default:
                info.tier = "Unranked";
             }
           }



           if (typeof info !== 'undefined' && info !== null)
             {
             if (info.queueType == "RANKED_SOLO_5x5") {
               callback(null, info.tier + " " + info.rank);
             }else{
               callback(null, "Unranked in solo");
             }
           }else{
             callback(null, "Unranked in solo");
           }


  	  })
  });
}
//GETLEAGUE FUNCTION VÉGE
//GETCHAMPIONS FUNCTION
function getChampions(championId, callback){
  var options = {
    url: 'http://ddragon.leagueoflegends.com/cdn/7.23.1/data/en_US/champion.json',
    headers: {
      "Origin": "https://developer.riotgames.com",
      "Accept-Charset": "application/x-www-form-urlencoded; charset=UTF-8",
      "X-Riot-Token": apiKey,
      "Accept-Language": "hu-HU,hu;q=0.9,en-US;q=0.8,en;q=0.7,sk;q=0.6,es;q=0.5,und;q=0.4,ru;q=0.3",
      "User-Agent": "Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/64.0.3282.140 Safari/537.36"
    }
  };

  request(options, function (error, response, body) {
    //console.log('error:', error); // Print the error if one occurred
    //console.log('statusCode:', response && response.statusCode); // Print the response status code if a response was received
    //console.log('body:', body); // Print the HTML for the Google homepage.
    //console.log(JSON.parse(body)); // Print the HTML for the Google homepage.
      process.nextTick(function(){
        var data = JSON.parse(body);
        var championName = "";
        for (var key in data.data) {
            //console.log("Champ: " + key);
            //console.log("champId: " + data.data[key].key);
            //champions += "\"" + data.data[key].key + "\": \""  + key + "\", ";
            if (championId == data.data[key].key){
              championName = key;
              championName = championName.replace(/([A-Z])/g, ' $1').trim()
            }
        }
        if(championId == -1){
          championName = "No ban";
        }
        callback(null, championName);



  	  })
  });
}
//GETCHAMPIONS FUNCTION VÉGE
io.on('connection', function(socket){
  console.log('Connected');
  socket.on('checkApiEmit', function(){
    sync(function(){
      var checkApi = checkApiFunc.sync(null);
      if (checkApi.status.status_code == 403) {
        socket.emit('checkApiSocket');
      }
    })
  });


  socket.on('search', function(data) {
    var summonerName = data.summonerName;
    console.log('Search hívása');

  sync(function(){
    var summonerId = getSummoner.sync(null, summonerName, data.server);
    if (summonerId == 0) {
      socket.emit('noSummoner');
      console.log("NoSummoner");
    }else{
      var match = getMatch.sync(null, summonerId, data.server);
      if (match == 0) {
        socket.emit('noMatch');
      }

      else{
        var summonerIds = [
          match.participants[0].summonerId,
          match.participants[1].summonerId,
          match.participants[2].summonerId,
          match.participants[3].summonerId,
          match.participants[4].summonerId,
          match.participants[5].summonerId,
          match.participants[6].summonerId,
          match.participants[7].summonerId,
          match.participants[8].summonerId,
          match.participants[9].summonerId,
         ];
      var summonerLeague = "";
      for (var i = 0; i < summonerIds.length; i++) {
        match.participants[i].rank = getLeague.sync(null, summonerIds[i], data.server)
        match.participants[i].championName = getChampions.sync(null, match.participants[i].championId)
        match.bannedChampions[i].championName = getChampions.sync(null, match.bannedChampions[i].championId)
      }
        console.log("Sending match");
        socket.emit('match', { match });
      }
    }
  })
  });
});





















server.listen(3000, function(){
  console.log('listening on *:3000');
});
